from python:latest

COPY ckan_topic_aggregator.py .

RUN pip3 install requests python-dateutil

CMD ["python3", "ckan_topic_aggregator.py"]
