import os
import urllib
import time
import logging
import datetime
import sys
import json
import dateutil.parser
import requests
import asyncio

from datetime import date, timedelta

# localhost setup - may need updated to what your env needs
CKAN_HOST = 'http://localhost:8000/api/3/action/package_show?id='
LINTOL_HOST = 'http://localhost:8000'
ELASTIC_SEARCH_API = 'http://172.18.0.1:9200/core/_doc/'
ELASTIC_SEARCH_API_LINTOL_ID = 'http://172.18.0.1:9200/core/_search?q=/resources/lintol_package_id='
REPORT_API = 'http://localhost:8000/apic/v1.0/reports?since='
DATA_RESOURCE_API = 'http://localhost:8000/apic/v1.0/dataResources?include=package&provider=_local&ids='

# # __live setup__ - previous links
# CKAN_HOST = 'https://ckan.ev.openindustry.in/api/3/action/package_show?id='
# LINTOL_HOST = 'https://ltl.ev.openindustry.in'
# ELASTIC_SEARCH_API = 'https://es.thedatatimes.com/core/_doc/'
# ELASTIC_SEARCH_API_LINTOL_ID = 'https://es.thedatatimes.com/core/_search?q=lintol_package_id:'
# REPORT_API ='https://ltl.ev.openindustry.in/apic/v1.0/reports?since='
# DATA_RESOURCE_API = 'https://ltl.ev.openindustry.in/apic/v1.0/dataResources?include=?include=package&provider=_local&ids='


# CONSTANTS
SINCE_HOURS = 4
RETRIES = 10
TIME_DELAY = 10
THRESHOLD = 50.0  # the minimum a dataset has to be related to word to be associated with that word.
DATA_RESOURCES_TOPICS = {}
DATA_RESOURCES_TYPES = {}
DATA_RESOURCES_NATURES = {}
DATA_RESOURCES_LOCS = {}
DATA_RESOURCES_ID_REPORTS_CREATED_AT = {}
DATA_RESOURCES_ID_LINTOL_PACKAGE_IDS = {}
LENGTH_OF_ATTRIBUTES = {}

def get_reports(url, since, until, resource_id=None):
    finished_reports = False
    reports = {}
    page = 1
    data_resources = {'data': [], 'included': []}
   
    #handling time period while reports are still loading
    while not finished_reports:
        endpoint = url + since + f'&until={until}&page={page}&filters[hasDataResource]=1'

        if resource_id:
            endpoint += f'&entity=resource&id={resource_id}'
        print("Resource id: ", resource_id, ". Endpoint is: ", endpoint)
        
        retry = 1
        connected = False
        #handling if the connection has been tried 1-10 times and connected is still not false
        #throwing exceptions when the json cant be read or there is a connection error
        while not connected and retry < RETRIES:

            try:
                results = requests.get(endpoint)
                new_reports = results.json()
         
                connected = True
            except json.decoder.JSONDecodeError:
                logging.error('Could not decode JSON...')
                retry += 1
                time.sleep(3)
            except requests.exceptions.RequestException:
                logging.error('Could not connect...')
                retry += 1
                time.sleep(3)

        # handling the situation when the number of report requires a new page -  breaking the connection
        if new_reports['meta']['pagination']['current_page'] < new_reports['meta']['pagination']['total_pages']:
            print(new_reports['meta']['pagination'])
            page += 1
            connected = False
        else: 
            #breaking the outter while - as the process has finished as expected
            finished_reports = True

        # now that the process has completed. we need to get the new resource keys & update the reports with the new ones   
        resource_keys = get_data_resources_ids(new_reports)
        reports.update(new_reports)
        # now we have the data we need upated and in resource keys
        # - we can link the new data to the existing arrays
        print('Get reports, resource keys: ', resource_keys)
        if resource_keys:
            new_data_resources = get_data_resources(DATA_RESOURCE_API, resource_keys)
            data_resources['data'] += new_data_resources['data']
            data_resources['included'] += new_data_resources['included']
     
    # return the updated reports and the resources 
    return reports, data_resources


def get_data_resources_ids(reports):
    '''
    Lintol creates reports based on data resources. The data resource contains information about the data
    and the ckan package information. To get the ckan package information the program gets the data resource ids
    from the report and maps that to topics from that report if the report contains topics processor
    '''

    print('Number of reports:', len(reports['data']))
    resource_keys = set()
    for report in reports['data']:
        # create aggregates for the attributes from the topics, types, natures and locs
        # store the d/t this was created
        if report['attributes']['dataResourceId']:
            data_resource_id = report['attributes']['dataResourceId']
            topics, types, natures, locs = aggregate_topics(report['attributes'])
            created_at = dateutil.parser.parse(report['attributes']['createdAt'])

            include = False
            LENGTH_OF_ATTRIBUTES = {"topics": [topics, len(topics)], 
                                    "types": [types, len(types)], 
                                    "natures": [natures,len(natures)], 
                                    "locs": [locs, len(locs)]
                                     }
            constant_vars = [DATA_RESOURCES_TOPICS, DATA_RESOURCES_TYPES,   
                             DATA_RESOURCES_NATURES, DATA_RESOURCES_LOCS]
            # x is for iteration through the constant variables
            x = 0
            for i in LENGTH_OF_ATTRIBUTES:
                if x < 4:
                    if LENGTH_OF_ATTRIBUTES[i][1] > 0 and (data_resource_id not in constant_vars[x]
                    or created_at > constant_vars[x][data_resource_id][1]):
                        include = True
                        constant_vars[x][data_resource_id] = (LENGTH_OF_ATTRIBUTES[i], created_at)
                x = x+1
                # if included is truthy/true - add the resource ids to the resource keys
                if include:
                    resource_keys.add(data_resource_id)

            # for each res id in the var, set the created at to .. max 
            if data_resource_id in DATA_RESOURCES_ID_REPORTS_CREATED_AT:
                created_at = max(created_at, DATA_RESOURCES_ID_REPORTS_CREATED_AT[data_resource_id])
            #storing created at in the dict var
            DATA_RESOURCES_ID_REPORTS_CREATED_AT[data_resource_id] = created_at

        # console prints to check the code ran correct/for feedback - pulled out into a method
        resource_keys_length = len(resource_keys)
        printDataResources(LENGTH_OF_ATTRIBUTES, resource_keys_length )
    #returning the resource keys
    return resource_keys


def printDataResources(LENGTH_OF_ATTRIBUTES, resource_keys_length):
    print("Number of data resources... ", resource_keys_length)
    print("Topics :", LENGTH_OF_ATTRIBUTES['topics'][1])
    print("Types :", LENGTH_OF_ATTRIBUTES['types'][1])
    print("Natures :", LENGTH_OF_ATTRIBUTES['natures'][1])
    print("Locs :", LENGTH_OF_ATTRIBUTES['locs'][1])
    
  
def aggregate_topics(attributes):
    '''
    Converts the topics processor output in to an array of topics associated with the data resource.
    '''
    all_topics = []
    all_types= []
    all_natures = [] 
    all_locs = []
    
    if 'content' in attributes:
        if isinstance(attributes['content'], str):
            content = json.loads(attributes['content'])
        else:
            content = attributes['content']

        if len(content['tables']) and len(content['tables'][0]['informations']) > 0:

           for information in content['tables'][0]['informations']:
               if information['code'] == 'possible-categories-all-weighted':
                   topics = extract_topics(information['error-data'])
                   all_topics = all_topics + topics
               elif information['code'] == 'additional-categories':
                   topics = information['error-data']
                   all_topics = all_topics + topics
               elif information['code'] == 'all-comprehension-type-tags':
                   natures = extract_natures(information['error-data'])
                   all_natures = all_natures + natures
               elif information['code'] == 'possible-locations-overall':
                   locs = extract_locs(information['error-data'])
                   all_locs = all_locs + locs

           if attributes['profile'] == 'Data Times Comprehender [test]':
               all_types = json.dumps({info['code']: info['error-data'] for info in content['tables'][0]['informations']})
    return list(set(all_topics)), all_types, all_natures, all_locs  # list removes duplicates


def extract_natures(error_data_array):
    return error_data_array


def extract_locs(error_data_array):
    locs = []
    for error_data in error_data_array:
        locs.append(error_data[0]['d']['name'].lower())
    return locs


def extract_topics(error_data_array):
    '''
    An example of the array to extract the topics from. It checks that the topic has a threshols of a precentage to be associated
    with the topic.
    E.G ["poverty:benefits", 0.24795256555080414]
    '''
    topics = []
    for error_data in error_data_array:
        related_topic = error_data[0]
        percentage = error_data[1]
        if percentage > THRESHOLD:
            topic = related_topic.split(':')
            topics = topics + topic
    return topics


def get_data_resources(url, ids):
    '''
    Get data resources from lintol based on an array of Ids
    '''
    connected = False
    print('Requesting the following number of data resources:', len(ids))
    ids_string = ','.join(ids)
    url = url + ids_string
    retry = 1
    while not connected and retry < RETRIES:
        try:
            results = requests.get(url)
            connected = True
        except requests.exceptions.RequestException:
            retry += 1
            logging.error('Could not connect...')
            time.sleep(3)
    data_resources = results.json()
    print('Number of Data Resources returned:', len(data_resources['data']))

    return data_resources


def set_ckan_package_ids(data_resources):
    '''
    create a mapping between data resource id and the lintol package ID.
    '''
    for data_resource in data_resources['data']:
        if 'package' in  data_resource['relationships']:
            DATA_RESOURCES_ID_LINTOL_PACKAGE_IDS[data_resource['id']] = data_resource['relationships']['package']['data']['id']
    print('Number of Lintol Package Ids Found:', len(DATA_RESOURCES_ID_LINTOL_PACKAGE_IDS))


def find_ckan_package(lintol_package_id, data_resources):
    '''
    Find ckan package information in the lintol data resources using the lintol package ID.
    '''
    for data_resource_included in data_resources['included']:
        if lintol_package_id == data_resource_included['id']:
            return data_resource_included['attributes']['metadata']
    return ''


def meld_data(data_resources):
    '''
    Meld ckan package and lintol information together to send to elastic search.
    '''
    exists = 0
    for idx, data_resource in enumerate(data_resources['data']):
        if not data_resource['id'] in DATA_RESOURCES_ID_REPORTS_CREATED_AT:
            continue
        print("MELD DATA")
        data_resource_id = data_resource['id']
        topics, _ = DATA_RESOURCES_TOPICS.get(data_resource_id, [None, None])
        types, _ = DATA_RESOURCES_TYPES.get(data_resource_id, [None, None])
        natures, _ = DATA_RESOURCES_NATURES.get(data_resource_id, [None, None])
        locs, _ = DATA_RESOURCES_LOCS.get(data_resource_id, [None, None])
        created_at = DATA_RESOURCES_ID_REPORTS_CREATED_AT[data_resource_id]
        print(created_at)
        lintol_package_id = DATA_RESOURCES_ID_LINTOL_PACKAGE_IDS[data_resource_id]
        result, elastic_search_id = does_package_exist(lintol_package_id)  
        if result:
            exists += 1
            ckan_package = find_ckan_package(lintol_package_id, data_resources)
            data = {}
            params = {"created_at": created_at.isoformat()}
            if topics:
                params["topics"] = topics
            if locs:
                params["locs"] = locs
            if types:
                params['types'] = types
            if natures:
                params['nature'] = natures
            if 'locale' in data_resource['attributes']:
                params['locale'] = data_resource['attributes']['locale']

            script = {
                "source": """
                if (ctx._source.created_at != params.created_at) {
                    ctx._source.created_at = params.created_at;
                    if (params.topics instanceof List) {
                        ctx._source.topics = params.topics;
                    }
                    if (params.locs instanceof List) {
                        ctx._source.locs = params.locs;
                    }
                    if (params.nature instanceof List) {
                        ctx._source.nature = params.nature;
                    }
                    if (params.types instanceof String) {
                        ctx._source.types = params.types;
                    }
                    ctx._source.locale = params.locale;
                }
                """,
                "lang": "painless",
                "params": params
            }
            data['script'] = script
            elastic_search_index = data
            update_in_elastic_search(elastic_search_id, elastic_search_index)
        else:
            ckan_package = find_ckan_package(lintol_package_id, data_resources)
            data = {} 
            try:
                #print("meld_data SOURCE", json.loads(data_resource['attributes']['source']))
                # Why did we need d json.loads? It causes a json error 'json.loads(....)
                source = data_resource['attributes']['source']
               
            except (json.decoder.JSONDecodeError, TypeError):
                print("JSON ERROR")             
                source = {
                    'lintolSource': data_resource['attributes']['source'],
                    'lintolCkanInstanceId': None,
                    'harvestTitle': '(unknown)'
                }
            data['created_at'] = created_at.isoformat()
            data['lintol_package_id'] = lintol_package_id
            data['topics'] = topics
            data['types'] = types
            data['nature'] = natures
            data['locs'] = locs
            
            if 'harvestTitle' in data:
                data['source_name'] = source['harvestTitle']

            # locale is at attribute level and not source level
            if 'locale' in data_resource['attributes']:
                data['locale'] = data_resource['attributes']['locale']

            # temporary code to set locale to 'en'
            # comment out the next 2 lines if you do not want the default to be 'en'
            if data['locale'] == None:
                data['locale'] = 'en'
                print("Default locale has been set.")
            
            if 'resources' in ckan_package:
                for resource in ckan_package['resources']:
                    if 'datastore_active' in resource and isinstance(resource['datastore_active'], str) is str:
                        resource['datastore_active'] = resource['datastore_active'].lower() == 'true'

            data['result'] = ckan_package
            elastic_search_index = data
            elastic_search_id = send_to_elastic_search(elastic_search_index)
    
        if types and 'data-sample' in types:
            print('Data Sample : ', elastic_search_id)

        time.sleep(0.3)

        if idx % 10 == 0:
            # completed?
            print(f'Competed {idx} / {len(data_resources["data"])} ({exists} updates)')

def does_package_exist(lintol_package_id):
    '''
    Checks if the ckan package already exists in elastic search. 
    lintol_package_id is the id and associated json 
    '''
    headers = {'Content-Type': 'application/json', 'Accept': 'text/plain'}
    retry = 1
    connected = False
    while not connected and retry < RETRIES:
        try:
            response = requests.get(url=ELASTIC_SEARCH_API_LINTOL_ID + lintol_package_id, headers=headers)  
            connected = True
        except requests.exceptions.RequestException:
            logging.error('Could not connect...')
            retry += 1
            time.sleep(3)
    result = response.json()

    if 'hits' in result:
        if result['hits']['total'] > 0:
            return True, result['hits']['hits'][0]['_id']
        else:
            return False, '0'
    else:
        print('No hits object available')
        return False, '0'


def send_to_elastic_search(data):
    '''
    Send the ckan package to elastic search to be searchable
    '''
    headers = {'Content-Type': 'application/json', 'Accept': 'text/plain'}
    retry = 1
    connected = False
    while not connected and retry < RETRIES:
        try:
            response = requests.post(url=ELASTIC_SEARCH_API, data=json.dumps(data), headers=headers)
            connected = True
        except requests.exceptions.RequestException:
            logging.error('Could not connect...')
            retry += 1
            time.sleep(3)
    if response.status_code == 201:
        print('Created document in index successfully')
        result = response.json()['_id']
    else:
        result = None
    return result


def update_in_elastic_search(elastic_search_id, data):
    '''
    Send the ckan package to elastic search to be searchable
    '''
    headers = {'Content-Type': 'application/json', 'Accept': 'text/plain'}
    retry = 1
    connected = False
    while not connected and retry < RETRIES:
        try:
            response = requests.post(url=ELASTIC_SEARCH_API + elastic_search_id + '/_update', data=json.dumps(data), headers=headers)
            connected = True
        except requests.exceptions.RequestException:
            logging.error('Could not connect...')
            retry += 1
            time.sleep(3)
    print(response.status_code)
    if response.status_code != 200:
        print('Failed to create document')
        print(response.json())

def run():
    if 'TDATIM_SINCE' in os.environ:
        if os.environ['TDATIM_SINCE'] == 'last' and os.path.exists('history'):
            with open('history', 'r') as f:
                since = int(float(f.readlines()[-1].split('-')[1].strip()))
        else:
            since = int(os.environ['TDATIM_SINCE'])
        since = datetime.datetime.fromtimestamp(since)
    else:
        since = (datetime.datetime.now() - datetime.timedelta(hours=SINCE_HOURS))

    if 'TDATIM_UNTIL' in os.environ:
        until = datetime.datetime.fromtimestamp(int(os.environ['TDATIM_UNTIL']))
    else:
        until = datetime.datetime.now()

    if 'TDATIM_RESOURCE_ID' in os.environ:
        resource_id = os.environ['TDATIM_RESOURCE_ID']
    else:
        resource_id = None

    since = since.strftime('%Y-%m-%dT%H:%M:%SZ')
    until = until.strftime('%Y-%m-%dT%H:%M:%SZ')
    print('Setting up to get reports from ' + since)
    print('   until ' + until)
    reports, data_resources = get_reports(REPORT_API, since, until, resource_id=resource_id)

#  could be updated to if length of reports is greater than 1 and has not been already processed?
    if len(reports['data']) > 0:
        set_ckan_package_ids(data_resources)
        meld_data(data_resources)
    else:
        print('No new reports to get')

    with open('history', 'a') as f:
        f.write('{}-{}\n'.format(*[datetime.datetime.timestamp(dateutil.parser.parse(t)) for t in (since, until)]))

if __name__ == "__main__":
    run()
